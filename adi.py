#!/usr/bin/env python
# -*- coding: utf-8 -*-
# /usr/share/zaproxy/zap.sh -daemon -host 0.0.0.0 -port 8080 -config api.key=123456789 -config api.addrs.addr.name=".*" -config api.addrs.addr.regex=true

import sys
import argparse
from core import banner
from core import zap
from core import simpletests
from thirdparty import xss
from thirdparty import sql

simpleTest = simpletests.simpleTests()
xssTest = xss.xssers()
sqlTest = sql.sqlers()
zapScanner = zap.zapscanner()

parser = argparse.ArgumentParser()
parser.add_argument("--target",help="target url",required=True)
parser.add_argument("--payload",help="payload url",default="http://www.google.com",required=False)
parser.add_argument("--xss",help="run xss tests",required=False,action='store_true')
parser.add_argument("--sqli",help="run sqli tests",required=False,action='store_true')
parser.add_argument("--zap",help="run zap scan",required=False,action='store_true')
parser.add_argument("--zapapi",help="zap aip key",default="123456789",required=False)
parser.add_argument("--zaplo",help="zap listening location",default="http://127.0.0.1:8080",required=False)

if len(sys.argv) == 1:
    banner.banner1()
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

if args.target:
    simpleTest.headerTests(args.target.strip(),args.payload.strip())
if args.zap:
    zapScanner.owaspZAP(args.target.strip(),args.zapapi,args.zaplo)
if args.sqli:
    sqlTest.dsss(args.target.strip())
# Must be last 
if args.xss:
    xssTest.xssScrapy(args.target.strip())
