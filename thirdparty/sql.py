open("thirdparty/DSSS/__init__.py","w")
from thirdparty.DSSS import dsss
class bclolors:
    RED="\033[01;31m"
    GREEN="\033[01;32m"    
    YELLOW="\033[01;33m"   
    BLUE="\033[01;34m"     
    BOLD="\033[01;01m"     
    RESET="\033[00m"

class sqlers():
    def dsss(self,app):
        print "\n"+bclolors.GREEN+"[*] "+bclolors.RESET+" launching DSSS against %s" % app
        result = dsss.scan_page(app if app.startswith("http") else "http://%s" % app)
        print "\nscan results: %s vulnerabilities found" % ("possible" if result else "no")
