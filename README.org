#+TITLE: AppDiscoverInject
I decided to script up things i do all the time when i test applications. This script leverages the [[https://github.com/zaproxy/zaproxy][OWASP ZAP]] proxy/scanner through the API, It also leverages [[https://github.com/DanMcInerney/xsscrapy][xsscrapy]] to identify potentiall XSS and [[https://github.com/stamparm/DSSS][DSSS]] for potential SQLi.
This script is nothing more than a wrapper of my favourite tools, i plan to add more tools :) .
* Acknowledgments
*These people did the hard stuff*
- [[https://github.com/zaproxy/zaproxy][zaproxy]]
- [[https://github.com/DanMcInerney/xsscrapy][xsscrapy]]
- [[https://github.com/stamparm/DSSS][DSSS]]
* Install
#+BEGIN_EXAMPLE
git clone --recursive https://github.com/jthorpe6/AppDiscoverInject && \
cd AppDiscoverInject && \
pip install -r ./requirements.txt
#+END_EXAMPLE
* ZAP api
This script can use the ZAP api to scan the application, but the zaproxy needs to be running first.
#+BEGIN_EXAMPLE
$ZAPINSTALLDIR/zap.sh -daemon -host 0.0.0.0 -port 8080 -config api.key=123456789 -config api.addrs.addr.name=".*" -config api.addrs.addr.regex=true
#+END_EXAMPLE
* Demonstration
** Help
#+BEGIN_EXAMPLE
root@1b3231a2d43b:~# python ./adi.py 

   _                 ___ _                           _____        _           _   
  /_\  _ __  _ __   /   (_)___  ___ _____   _____ _ _\_   \_ __  (_) ___  ___| |_ 
 //_\| '_ \| '_ \ / /\ / / __|/ __/ _ \ \ / / _ \ '__/ /\/ '_ \ | |/ _ \/ __| __|
/  _  \ |_) | |_) / /_//| \__ \ (_| (_) \ V /  __/ /\/ /_ | | | || |  __/ (__| |_ 
\_/ \_/ .__/| .__/___,' |_|___/\___\___/ \_/ \___|_\____/ |_| |_|/ |\___|\___|\__|
      |_|   |_|                                                |__/               


usage: adi.py [-h] --target TARGET [--payload PAYLOAD] [--zap]
              [--zapapi ZAPAPI] [--zaplo ZAPLO]

optional arguments:
  -h, --help         show this help message and exit
  --target TARGET    target url
  --payload PAYLOAD  payload url
  --zap              run zap scan
  --zapapi ZAPAPI    zap aip key
  --zaplo ZAPLO      zap listening location
#+END_EXAMPLE
** Quick scan
#+BEGIN_EXAMPLE
root@85dc1dcaa687:~/AppDiscoverInject# python ./adi.py --target http://192.168.1.254

[*] Searching for cookie values in http://192.168.1.254
..snip..

[*] Testing injection of http://www.google.com in the Host header to http://192.168.1.254
..snip..

[*] Trying POST request to http://192.168.1.254 with http://www.google.com as the referer header
..snip..

[*] Trying GET request to http://192.168.1.254 with http://www.google.com as the origin header
..snip..

2017-10-31 16:09:17 [scrapy] INFO: Scrapy 1.1.0rc3 started (bot: xsscrapy)
2017-10-31 16:09:17 [scrapy] INFO: Overridden settings: {'NEWSPIDER_MODULE': 'xsscrapy.spiders', 'DUPEFILTER_CLASS': 'xsscrapy.bloomfilters.BloomURLDupeFilter', 'SPIDER_MODULES': ['xsscrapy.spiders'], 'CONCURRENT_REQUESTS': 30, 'BOT_NAME': 'xsscrapy'}
2017-10-31 16:09:17 [scrapy] INFO: Enabled extensions:
['scrapy.extensions.logstats.LogStats',
 'scrapy.extensions.telnet.TelnetConsole',
 'scrapy.extensions.corestats.CoreStats']
2017-10-31 16:09:17 [scrapy] INFO: Enabled downloader middlewares:
['xsscrapy.middlewares.InjectedDupeFilter',
..snip..

#+END_EXAMPLE
The script will first try inject the value of ~--payload~ (default http://www.google.com) into various headers and report back. 
** Leveraging the ZAP scanner
After launching OWASP Zap and configuring the [[https://github.com/zaproxy/zaproxy/wiki/ApiDetails][api]] so you can connect to it launch it against the target with the ~--zap~ option, should your apikey and location be different, use the ~--zapapi~ and ~--zaplo~ options
#+BEGIN_EXAMPLE
root@85dc1dcaa687:~/AppDiscoverInject# python ./adi.py --target http://192.168.1.254 --zap --zapapi 123456789 --zaplo http://192.168.1.3:8080
[+] Accessing target http://192.168.1.254
[+] Spidering target http://192.168.1.254
[+] Spider progress %: 45
[+] Spider completed
[+] Scanning target http://192.168.1.254
[+] Scan progress %: 0
[+] Scan progress %: 2
[+] Scan progress %: 5
[+] Scan progress %: 7
..snip..
#+END_EXAMPLE
After scanning with ZAP, the results will be written to the working directory
* why another web app scanner ?
Few reasons 
- i wanted to improve my skills in python
- i want to automate things i do all the time
- i wanted to give back to the community

